############ DO NOT CHANGE THESE #################
PATH_DIV:=./srt_radix4_divider
PATH_MUL:=./pipelined_multiplier
TOP_MODULE?= mk_srt_radix4_divider

############# User changes to be done below #################
TOP_DIR?= srt_radix4_divider
TOP_FILE?= srt_radix4_divider_64.bsv

