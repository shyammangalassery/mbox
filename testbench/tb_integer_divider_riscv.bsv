/*
see LICENSE.iitm

--------------------------------------------------------------------------------------------------
*/
package tb_integer_divider_riscv;
import divider::*;
import RegFile::*;
import riscv_types::*;
`include "defined_parameters.bsv"

(*synthesize*)
module mk_tb_for_integer_divider_riscv(Empty);

	Reg#(Bit#(5)) rg_counter <- mkReg(0);	//Counter to get values from each line in Regfile
	Reg#(Bit#(32)) rg_clock <-mkReg(0);     //Clock register

	Ifc_divider divider <- mkdivider;


	Reg#(Bit#(32)) rg_instruction <-mkReg(32'b00011_00010_00001_0000001_111_0110011); //register to hold the 32 bit instruction

	rule rl_increment;
		
		rg_clock<=rg_clock+1;
		//$display("CLOCK=%d",rg_clock);

		if(rg_clock=='d70)
			$finish(0);
	endrule:rl_increment
   
	rule rl_give_input(rg_clock>'d4);
		
		ALU_func operation = DIV;

		//divider.input_operands('h00007e7bc2f6dc23,'h000000023f289c23,operation,0,'d24);
		if(rg_clock == 'd5)
			divider.ma_start('h0000000000000000,'h0000000000000000,DIV);
		if(rg_clock == 'd70)
			divider.ma_start('h7a97378bc3d6a823,'h0000000bc2f6dc23,operation);
		if(rg_clock == 'd75)
			divider.ma_start('h7a97378bc3d6a823,'h0000000bc2f6dc23,operation);
		//		(_dividend, _divisor ,_Two_bits_for_DIVtype,_word_instruction_flag)
				//Set word_instruction_flag to 0 for non rv64 instructions

		rg_counter <= rg_counter +1; //rg_counter increment to read next line

	endrule:rl_give_input

	rule rl_push_flush;
		if(rg_clock == 'd74)
			divider.ma_set_flush;
	endrule

	rule rl_get_output;

		
		$display("CLOCK=%d",rg_clock);
		$display("The output is %h", divider.mav_result); 
		
		//divider._release();  //release the FIFO's after getting the output
	endrule
endmodule : mk_tb_for_integer_divider_riscv
endpackage: tb_integer_divider_riscv
