# CHANGELOG
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.7] - 2021-12-31
## Added
- piplelined_multiplier: added 3 pipelined wallace multipliers (4 stages, 4 stages with retiming, one with CLA in last stage), added testbenches for the files

## [1.0.6] - 2021-10-06
## Fixed
- piplelined_multiplier: added 2 pipelined multipliers (4 stages, 5 stages), edited testbench for testing all opcodes
- parameters: moved parameters to base directory

## [1.0.5] - 2021-08-16
## Fixed
- repo: clean up directories, add/edit license

## [1.0.4] - 2021-06-25
## Fixed
- baseline_multiplier: latency set to 5 cycles (based on i-class synthesis results)

## [1.0.3] - 2021-05-04
## Fixed
- baseline_multiplier: latency set to 4 cycles

## [1.0.2] - 2021-04-15
## Added
- Srt_radix4_divider: Separate non-parameterized version for div_width=64
- Testbench: Testbench for the non-parameterized version 

## [1.0.1] - 2021-04-10
## Added
- changelog: created
- ci: edited

