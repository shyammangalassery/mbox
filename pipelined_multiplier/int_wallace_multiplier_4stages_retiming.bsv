/*
see LICENSE.iitm

Author : Nagakaushik Moturi
Email id : ee17b111@smail.iitm.ac.in
Details : This module implements the multiplier for RISC-V. It expects the operands and funct3
arguments to be provided. It is pipelined version with 4 stages. It follows the Wallace Tree Multiplier
algorithm by using carry save adders in each reduction stage.
Additionaly, this module has been designed for retiming. This allows for 
the synthesizer to retime the circuit by moving around the registers within the combo blocks
------------------------------------------------------------------------------------------------------
*/

package int_wallace_multiplier_4stages_retiming;
  import DReg :: *;
  import Vector :: * ;
  `include "Logger.bsv"
  `include "mbox_parameters.bsv"

  interface Ifc_int_wallace_multiplier;
    (*always_ready*)
		method Action send(Bit#(`XLEN) in1, Bit#(`XLEN) in2, Bit#(3) funct3
                                                                `ifdef RV64, Bool word32 `endif );
		method Tuple2#(Bit#(1),Bit#(64)) receive;
	endinterface
	
	(*synthesize*)
	module mk_int_wallace_multiplier (Ifc_int_wallace_multiplier);
	  
	  Reg#(Bit#(65)) rg_operands1 <- mkReg(0);
	  Reg#(Bit#(65)) rg_operands2 <- mkReg(0);
    
    Reg#(Bit#(130)) rg_out   <- mkReg(0);
    Reg#(Bit#(130)) rg_out_1 <- mkReg(0);
    Reg#(Bit#(130)) rg_out_2 <- mkReg(0);
    
    Reg#(Bit#(3)) rg_funct3_0 <- mkReg(0);
    Reg#(Bit#(3)) rg_funct3_1 <- mkReg(0);
    Reg#(Bit#(3)) rg_funct3_2 <- mkReg(0);
    Reg#(Bit#(3)) rg_fn3 <- mkReg(0);
    
    Reg#(Bool) rg_word <- mkReg(False);
    Reg#(Bool) rg_word_1 <- mkReg(False);
    Reg#(Bool) rg_word_2 <- mkReg(False);
    Reg#(Bool) rg_word_3 <- mkReg(False);
    
    Reg#(Bit#(1)) rg_valid <- mkReg(0);
    Reg#(Bit#(1)) rg_valid_1 <- mkReg(0);
    Reg#(Bit#(1)) rg_valid_2 <- mkReg(0);
    Reg#(Bit#(1)) rg_valid_3 <- mkReg(0);
    
    Reg#(Bit#(1)) rg_sign <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_1 <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_2 <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_3 <- mkReg(0);

    
    
	  
	  //csa_x_y : Carry sum addition of x bit operands seperated by y bits, producing output of required length
	  
	  function Vector#(2, Bit#(67)) csa_65_1 (Bit#(65) a,Bit#(65) b,Bit#(65) c);  // CSA for 65 bit inputs seperated by 1 bit
	    Bit#(67) a1,b1,c1;
	    Vector#(2, Bit#(67)) out;
	    
	    a1 = {a,2'd0};
	    b1 = {1'd0,b,1'd0};
	    c1 = {2'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    function Vector#(2, Bit#(73)) csa_67_3 (Bit#(67) a,Bit#(67) b,Bit#(67) c);  // CSA for 67 bit inputs seperated by 3 bits
	    Bit#(73) a1,b1,c1;
	    Vector#(2, Bit#(73)) out;
	    
	    a1 = {a,6'd0};
	    b1 = {3'd0,b,3'd0};
	    c1 = {6'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    function Vector#(2, Bit#(74)) csa_73_0 (Bit#(73) a,Bit#(73) b,Bit#(73) c);  // CSA for 73 bit inputs seperated by 0 bits
      Vector#(2, Bit#(74)) out;
      out[0]={1'b0,a^b^c};
      out[1]={a&b|b&c|c&a,1'b0};
      return out;
    endfunction
    
    function Vector#(2, Bit#(91)) csa_73_9 (Bit#(73) a,Bit#(73) b,Bit#(73) c);  // CSA for 73 bit inputs seperated by 9 bits
	    Bit#(91) a1,b1,c1;
	    Vector#(2, Bit#(91)) out;
	    
	    a1 = {a,18'd0};
	    b1 = {9'd0,b,9'd0};
	    c1 = {18'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    function Vector#(2, Bit#(92)) csa_74_9 (Bit#(74) a,Bit#(74) b,Bit#(74) c);  // CSA for 74 bit inputs seperated by 9 bits
	    Bit#(92) a1,b1,c1;
	    Vector#(2, Bit#(92)) out;
	    
	    a1 = {a,18'd0};
	    b1 = {9'd0,b,9'd0};
	    c1 = {18'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    
	  
	  rule partial_production_reduction_tree;
	    Bit#(67) csa_l1_1[21],csa_l1_2[21];    //variables for storing CSA outputs from first reduction stage
	    //l1_1 for storing sums and l1_2 for carries
	    //first stage inputs are the direct partial products generated by ANDing one input by each bit of other input
	    
	    for (Integer i=0; i<21; i=i+1) begin
	      csa_l1_1[i] = csa_65_1((rg_operands1&signExtend(rg_operands2[3*i+2])),(rg_operands1&signExtend(rg_operands2[3*i+1])),(rg_operands1&signExtend(rg_operands2[3*i])))[0];
	      csa_l1_2[i] = csa_65_1((rg_operands1&signExtend(rg_operands2[3*i+2])),(rg_operands1&signExtend(rg_operands2[3*i+1])),(rg_operands1&signExtend(rg_operands2[3*i])))[1];
	      end
	    
	    Bit#(73) csa_l2_1[7],csa_l2_2[7],csa_l2_3[7],csa_l2_4[7];        //variables for storing CSA outputs from second reduction stage
	    //l2_1 and l2_2 for sum and carry of l2_3 and l2_4 for sum and carry for l1_2 
	    
	    for (Integer i=0; i<7; i=i+1) begin
	      csa_l2_1[i] = csa_67_3 (csa_l1_1[3*i+2],csa_l1_1[3*i+1],csa_l1_1[3*i])[0];
	      csa_l2_2[i] = csa_67_3 (csa_l1_2[3*i+2],csa_l1_2[3*i+1],csa_l1_2[3*i])[0];
	      csa_l2_3[i] = csa_67_3 (csa_l1_1[3*i+2],csa_l1_1[3*i+1],csa_l1_1[3*i])[1];
	      csa_l2_4[i] = csa_67_3 (csa_l1_2[3*i+2],csa_l1_2[3*i+1],csa_l1_2[3*i])[1];
	      end
	    
	    Bit#(74) csa_l3_1[7],csa_l3_2[7];                               //variables for storing CSA outputs from third reduction stage
	    Bit#(91) csa_l3_3[2],csa_l3_4[2];
	    Bit#(76) csa_l3_5[2];
	    
	    for (Integer i=0; i<7; i=i+1) begin
	      csa_l3_1[i] = csa_73_0 (csa_l2_1[i],csa_l2_2[i],csa_l2_3[i])[0];
	      csa_l3_2[i] = csa_73_0 (csa_l2_1[i],csa_l2_2[i],csa_l2_3[i])[1];
	      end
	    csa_l3_3[0] = csa_73_9 (csa_l2_4[2],csa_l2_4[1],csa_l2_4[0])[0];
	    csa_l3_4[0] = csa_73_9 (csa_l2_4[2],csa_l2_4[1],csa_l2_4[0])[1];
	    csa_l3_3[1] = csa_73_9 (csa_l2_4[5],csa_l2_4[4],csa_l2_4[3])[0];
	    csa_l3_4[1] = csa_73_9 (csa_l2_4[5],csa_l2_4[4],csa_l2_4[3])[1];
	    
	    //Carry Save Addition (producing sum & carry)
	    csa_l3_5[0] = {3'b0,csa_l2_4[6]}^{2'b0,(rg_operands1&signExtend(rg_operands2[63])),9'd0}^{1'b0,(rg_operands1&signExtend(rg_operands2[64])),10'd0};
	    csa_l3_5[1] = ({3'b0,csa_l2_4[6]}&{2'b0,(rg_operands1&signExtend(rg_operands2[63])),9'd0}|{2'b0,(rg_operands1&signExtend(rg_operands2[63])),9'd0}&{1'b0,(rg_operands1&signExtend(rg_operands2[64])),10'd0}|{3'b0,csa_l2_4[6]}&{1'b0,(rg_operands1&signExtend(rg_operands2[64])),10'd0})<<1;
	    
	    Bit#(92) csa_l4_1[2],csa_l4_2[2],csa_l4_3[2],csa_l4_4[2];       //variables for storing CSA outputs from fourth reduction stage
	    Bit#(91) csa_l4_5[2];
	    Bit#(119) csa_l4_6[2];
	    
	    csa_l4_1[0] = csa_74_9 (csa_l3_1[3],csa_l3_1[2],csa_l3_1[1])[0];
	    csa_l4_2[0] = csa_74_9 (csa_l3_1[3],csa_l3_1[2],csa_l3_1[1])[1];
	    csa_l4_3[0] = csa_74_9 (csa_l3_2[3],csa_l3_2[2],csa_l3_2[1])[0];
	    csa_l4_4[0] = csa_74_9 (csa_l3_2[3],csa_l3_2[2],csa_l3_2[1])[1];
	    
	    csa_l4_1[1] = csa_74_9 (csa_l3_1[6],csa_l3_1[5],csa_l3_1[4])[0];
	    csa_l4_2[1] = csa_74_9 (csa_l3_1[6],csa_l3_1[5],csa_l3_1[4])[1];
	    csa_l4_3[1] = csa_74_9 (csa_l3_2[6],csa_l3_2[5],csa_l3_2[4])[0];
	    csa_l4_4[1] = csa_74_9 (csa_l3_2[6],csa_l3_2[5],csa_l3_2[4])[1];
	    
	    //Carry Save Addition (producing sum & carry)
	    
	    csa_l4_5[0] = {17'b0,csa_l3_1[0]}^{17'b0,csa_l3_2[0]}^csa_l3_3[0];
	    csa_l4_5[1] = ({17'b0,csa_l3_1[0]}&{17'b0,csa_l3_2[0]}|{17'b0,csa_l3_2[0]}&csa_l3_3[0]|{17'b0,csa_l3_1[0]}&csa_l3_3[0])<<1;
	    
	    csa_l4_6[0] = {1'b0,csa_l3_3[1],27'd0}^{1'b0,csa_l3_4[1],27'd0}^{28'b0,csa_l3_4[0]};
	    csa_l4_6[1] = {{csa_l3_3[1],27'd0}&{csa_l3_4[1],27'd0}|{csa_l3_4[1],27'd0}&{27'b0,csa_l3_4[0]}|{csa_l3_3[1],27'd0}&{27'b0,csa_l3_4[0]},1'b0};
	    
	    Bit#(93) csa_l5_1[2], csa_l5_2[2];                              //variables for storing CSA outputs from fifth reduction stage
	    Bit#(119) csa_l5_3[2];
	    Bit#(128) csa_l5_4[2];
	    
	    csa_l5_1[0] = {1'b0,csa_l4_1[0]^csa_l4_2[0]^csa_l4_3[0]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_2[0] = {(csa_l4_1[0]&csa_l4_2[0])|(csa_l4_2[0]&csa_l4_3[0])|(csa_l4_1[0]&csa_l4_3[0]),1'b0};
	    
	    csa_l5_1[1] = {1'b0,csa_l4_1[1]^csa_l4_2[1]^csa_l4_3[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_2[1] = {(csa_l4_1[1]&csa_l4_2[1])|(csa_l4_2[1]&csa_l4_3[1])|(csa_l4_1[1]&csa_l4_3[1]),1'b0};
	    
	    csa_l5_3[0] = csa_l4_6[0]^{28'b0,csa_l4_5[0]}^{28'b0,csa_l4_5[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_3[1] = (csa_l4_6[0]&{28'b0,csa_l4_5[0]}|{28'b0,csa_l4_5[0]}&{28'b0,csa_l4_5[1]}|csa_l4_6[0]&{28'b0,csa_l4_5[1]})<<1;
	    
	    csa_l5_4[0] = {27'b0,csa_l4_4[0],9'b0}^{csa_l4_4[1],36'b0}^{9'b0,csa_l4_6[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_4[1] = ({27'b0,csa_l4_4[0],9'b0}&{csa_l4_4[1],36'b0}|{csa_l4_4[1],36'b0}&{9'b0,csa_l4_6[1]}|{27'b0,csa_l4_4[0],9'b0}&{9'b0,csa_l4_6[1]})<<1;
	    
	    Bit#(120) csa_l6_1[2];                                             //variables for storing CSA outputs from sixth reduction stage
	    Bit#(129) csa_l6_2[2];
	    Bit#(130) csa_l6_3[2];
	    
	    csa_l6_1[0] = {csa_l5_1[1],27'b0}^{27'b0,csa_l5_1[0]}^{27'b0,csa_l5_2[0]}; //Carry Save Addition (producing sum & carry)
	    csa_l6_1[1] = ({csa_l5_1[1],27'b0}&{27'b0,csa_l5_1[0]}|{27'b0,csa_l5_1[0]}&{27'b0,csa_l5_2[0]}|{csa_l5_1[1],27'b0}&{27'b0,csa_l5_2[0]})<<1;
	    
	    csa_l6_2[0] = {csa_l5_2[1],36'b0}^{10'b0,csa_l5_3[0]}^{10'b0,csa_l5_3[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l6_2[1] = ({csa_l5_2[1],36'b0}&{10'b0,csa_l5_3[0]}|{10'b0,csa_l5_3[0]}&{10'b0,csa_l5_3[1]}|{csa_l5_2[1],36'b0}&{10'b0,csa_l5_3[1]})<<1;
	    
	    csa_l6_3[0] = {2'b0,csa_l5_4[0]}^{2'b0,csa_l5_4[1]}^{csa_l3_5[0],54'd0}; //Carry Save Addition (producing sum & carry)
	    csa_l6_3[1] = ({2'b0,csa_l5_4[0]}&{2'b0,csa_l5_4[1]}|{2'b0,csa_l5_4[1]}&{csa_l3_5[0],54'd0}|{2'b0,csa_l5_4[0]}&{csa_l3_5[0],54'd0})<<1;

	    
	    Bit#(130) csa_l7_1[2],csa_l7_2[2];                               //variables for storing CSA outputs from seventh reduction stage
	    
	    csa_l7_1[0] = {1'b0,csa_l6_1[0],9'b0}^{1'b0,csa_l6_1[1],9'b0}^{1'b0,csa_l6_2[0]}; //Carry Save Addition (producing sum & carry)
	    csa_l7_1[1] = ({1'b0,csa_l6_1[0],9'b0}&{1'b0,csa_l6_1[1],9'b0}|{1'b0,csa_l6_1[1],9'b0}&{1'b0,csa_l6_2[0]}|{1'b0,csa_l6_1[0],9'b0}&{1'b0,csa_l6_2[0]})<<1;
	    
	    csa_l7_2[0] = {1'b0,csa_l6_2[1]}^csa_l6_3[0]^csa_l6_3[1]; //Carry Save Addition (producing sum & carry)
	    csa_l7_2[1] = ({1'b0,csa_l6_2[1]}&csa_l6_3[0]|csa_l6_3[0]&csa_l6_3[1]|{1'b0,csa_l6_2[1]}&csa_l6_3[1])<<1;
	    
	    Bit#(130) csa_l8[2];                                             //variables for storing CSA outputs from eigth reduction stage
	    
	    csa_l8[0] = csa_l7_1[0]^csa_l7_1[1]^csa_l7_2[0]; //Carry Save Addition (producing sum & carry)
	    csa_l8[1] = (csa_l7_1[0]&csa_l7_1[1]|csa_l7_1[1]&csa_l7_2[0]|csa_l7_1[0]&csa_l7_2[0])<<1;
	    
	    Bit#(130) csa_l9[2];                                             //variables for storing CSA outputs from ninth reduction stage
	    
	    csa_l9[0] = csa_l8[0]^csa_l8[1]^csa_l7_2[1]; //Carry Save Addition (producing sum & carry)
	    csa_l9[1] = (csa_l8[0]&csa_l8[1]|csa_l8[1]&csa_l7_2[1]|csa_l8[0]&csa_l7_2[1])<<1;
	    
	    Bit#(130) csa_l10[2];                                            //variables for storing CSA outputs from tenth reduction stage
	    
	    csa_l10[0] = csa_l9[0]^csa_l9[1]^{csa_l3_5[1],54'b0}; //Carry Save Addition (producing sum & carry)
	    csa_l10[1] = (csa_l9[0]&csa_l9[1]|csa_l9[1]&{csa_l3_5[1],54'b0}|csa_l9[0]&{csa_l3_5[1],54'b0})<<1;
	    
	    rg_out <= csa_l10[0]+csa_l10[1];                 //carry look ahead adder
	    
	    rg_funct3_1 <= rg_funct3_0 ;
      rg_word_1 <= rg_word ;
      rg_valid_1 <= rg_valid ;
      rg_sign_1 <= rg_sign;
      
	  endrule
	  
	  rule dummy_stage_for_retiming_1;
	    rg_out_1 <= rg_out;
	    rg_funct3_2 <= rg_funct3_1 ;
      rg_word_2 <= rg_word_1 ;
      rg_valid_2 <= rg_valid_1 ;
      rg_sign_2 <= rg_sign_1;
	  endrule
	  
	  rule dummy_stage_for_retiming_2;
	    rg_out_2 <= rg_out_1 ;
	    rg_fn3 <= rg_funct3_2 ;
      rg_word_3 <= rg_word_2 ;
      rg_valid_3 <= rg_valid_2 ;
      rg_sign_3 <= rg_sign_2;
	  endrule
	  
	  //sending operands after converting them to positive operands, sending opcode
    method Action send(Bit#(`XLEN) in1, Bit#(`XLEN) in2, Bit#(3) funct3
                                                              `ifdef RV64, Bool word32 `endif );
      Bit#(1) sign1 = funct3[1]^funct3[0];
      Bit#(1) sign2 = pack(funct3[1 : 0] == 1);
      let op1 = unpack({sign1 & in1[valueOf(`XLEN) - 1], in1});
      let op2 = unpack({sign2 & in2[valueOf(`XLEN) - 1], in2});
      
      Bit#(65) opp1 =0;
      Bit#(65) opp2 =0;
      
      //sending the positive version of operands (making them positive if they are negative)
      if ((sign1 & in1[valueOf(`XLEN) - 1])==0) begin opp1 = op1; end
      else begin opp1 = (~op1)+65'd1; end
      if ((sign2 & in2[valueOf(`XLEN) - 1])==0) begin opp2 = op2; end
      else begin opp2 = (~op2)+65'd1; end
      rg_operands1 <= opp1;
      rg_operands2 <= opp2;
      
      rg_funct3_0 <= funct3;
      
      //determining the sign of the final product to correct it in the final stage
      rg_sign <= (sign2 & in2[valueOf(`XLEN) - 1])^(sign1 & in1[valueOf(`XLEN) - 1]);
    `ifdef RV64
      rg_word <= word32;
    `endif
     rg_valid<=1;
    
    endmethod
    
    //Giving appropriate sign to the product, determining which bits to send, method for receiving the output and valid bit
    method Tuple2#(Bit#(1),Bit#(64)) receive;
    
      Bool lv_upperbits = unpack(|rg_fn3[1:0]);  //determining whether the upper XLEN bits or lower XLEN bits to send according to the funct3
      
      Bit#(`XLEN) default_out;
      Bit#(130) out;
      
      if (rg_sign_3==1)
        out = ~rg_out_2+1;
      else
        out = rg_out_2;
      
      if (lv_upperbits) begin
        default_out = out[valueOf(TMul#(2, `XLEN)) - 1 : valueOf(`XLEN)];
        end
      else
        default_out = out[valueOf(`XLEN) - 1:0];

      `ifdef RV64            //implementing RV64 MULW
        if(rg_word_3)
          default_out = signExtend(default_out[31 : 0]);
      `endif
      return tuple2(rg_valid_3,default_out);
    endmethod
    
  endmodule
endpackage
