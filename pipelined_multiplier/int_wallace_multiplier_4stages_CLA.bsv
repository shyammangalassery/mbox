/*
see LICENSE.iitm

Author : Nagakaushik Moturi
Email id : ee17b111@smail.iitm.ac.in
Details : This module implements the multiplier for RISC-V. It expects the operands and funct3
arguments to be provided. It is pipelined version with 4 stages. It follows the Wallace Tree Multiplier
algorithm by using carry save adders in each reduction stage.Uses CLA in the last stage for addition.
------------------------------------------------------------------------------------------------------
*/

package int_wallace_multiplier_4stages_CLA;
  import DReg :: *;
  import Vector :: * ;
  `include "mbox_parameters.bsv"  

  interface Ifc_int_wallace_multiplier;
    (*always_ready*)
		method Action send(Bit#(`XLEN) in1, Bit#(`XLEN) in2, Bit#(3) funct3
                                                                `ifdef RV64, Bool word32 `endif );
		method Tuple2#(Bit#(1),Bit#(64)) receive;
	endinterface
	
	(*synthesize*)
	module mk_int_wallace_multiplier (Ifc_int_wallace_multiplier);
    
    Reg#(Bit#(130)) rg_out   <- mkReg(0);
    Reg#(Bit#(130)) rg_out_1 <- mkReg(0);
    Reg#(Bit#(130)) rg_out_2 <- mkReg(0);
    
    Reg#(Bit#(3)) rg_funct3_0 <- mkReg(0);
    Reg#(Bit#(3)) rg_funct3_1 <- mkReg(0);
    Reg#(Bit#(3)) rg_funct3_2 <- mkReg(0);
    Reg#(Bit#(3)) rg_fn3 <- mkReg(0);
    
    Reg#(Bool) rg_word <- mkReg(False);
    Reg#(Bool) rg_word_1 <- mkReg(False);
    Reg#(Bool) rg_word_2 <- mkReg(False);
    Reg#(Bool) rg_word_3 <- mkReg(False);
    
    Reg#(Bit#(1)) rg_valid <- mkReg(0);
    Reg#(Bit#(1)) rg_valid_1 <- mkReg(0);
    Reg#(Bit#(1)) rg_valid_2 <- mkReg(0);
    Reg#(Bit#(1)) rg_valid_3 <- mkReg(0);
    
    Reg#(Bit#(1)) rg_sign <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_1 <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_2 <- mkReg(0);
    Reg#(Bit#(1)) rg_sign_3 <- mkReg(0);
    
    Vector#(4, Reg#(Bit#(65))) rg_operands1 <- replicateM(mkReg(0));
    Vector#(4, Reg#(Bit#(65))) rg_operands2 <- replicateM(mkReg(0));
    
    //Registers for storing CSA outputs from fifth reduction stage
    Reg#(Bit#(93)) csa_l5_1[2], csa_l5_2[2];                              
	  Reg#(Bit#(119)) csa_l5_3[2];
	  Reg#(Bit#(128)) csa_l5_4[2];
    
    for (Integer i=0; i<2; i=i+1) begin
      csa_l5_1[i] <- mkReg(0);
      csa_l5_2[i] <- mkReg(0);
      csa_l5_3[i] <- mkReg(0);
      csa_l5_4[i] <- mkReg(0);
      end
      
    Reg#(Bit#(76)) rg_csa_l3_5_0 <- mkReg(0); //Registers for storing output of 3rd reduction stage to use in next rule
    Reg#(Bit#(76)) rg_csa_l3_5_1 <- mkReg(0);
    
    Reg#(Bit#(130)) csa_l10[2];        // Registers for storing CSA outputs from tenth reduction stage
    csa_l10[0] <- mkReg(0);
    csa_l10[1] <- mkReg(0);
    
    
	  //csa_x_y : Carry sum addition of x bit operands seperated by y bits, producing output of required length
	  
	  function Vector#(2, Bit#(67)) csa_65_1 (Bit#(65) a,Bit#(65) b,Bit#(65) c);  // CSA for 65 bit inputs seperated by 1 bit
	    Bit#(67) a1,b1,c1;
	    Vector#(2, Bit#(67)) out;
	    
	    a1 = {a,2'd0};
	    b1 = {1'd0,b,1'd0};
	    c1 = {2'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    function Vector#(2, Bit#(73)) csa_67_3 (Bit#(67) a,Bit#(67) b,Bit#(67) c);  // CSA for 67 bit inputs seperated by 3 bits
	    Bit#(73) a1,b1,c1;
	    Vector#(2, Bit#(73)) out;
	    
	    a1 = {a,6'd0};
	    b1 = {3'd0,b,3'd0};
	    c1 = {6'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    function Vector#(2, Bit#(74)) csa_73_0 (Bit#(73) a,Bit#(73) b,Bit#(73) c);  // CSA for 73 bit inputs seperated by 0 bits
      Vector#(2, Bit#(74)) out;
      out[0]={1'b0,a^b^c};
      out[1]={a&b|b&c|c&a,1'b0};
      return out;
    endfunction
    
    function Vector#(2, Bit#(91)) csa_73_9 (Bit#(73) a,Bit#(73) b,Bit#(73) c);  // CSA for 73 bit inputs seperated by 9 bits
	    Bit#(91) a1,b1,c1;
	    Vector#(2, Bit#(91)) out;
	    
	    a1 = {a,18'd0};
	    b1 = {9'd0,b,9'd0};
	    c1 = {18'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    function Vector#(2, Bit#(92)) csa_74_9 (Bit#(74) a,Bit#(74) b,Bit#(74) c);  // CSA for 74 bit inputs seperated by 9 bits
	    Bit#(92) a1,b1,c1;
	    Vector#(2, Bit#(92)) out;
	    
	    a1 = {a,18'd0};
	    b1 = {9'd0,b,9'd0};
	    c1 = {18'd0,c};
	    
	    out[0] = a1^b1^c1;
	    out[1] = ((a1&b1)|(b1&c1)|(c1&a1))<<1;
	    
	    return out;
    endfunction
    
    function Bit#(2) carry_gen (Bit#(1) a,Bit#(1) b);  //generating the carry from the input operands
	    Bit#(2) c=2'b00;
	    // 00 - kill
	    // 01 - propagate
	    // 10 - generate
	    if ((a==0)&&(b==0)) c = 2'b00;
	    if (((a==0)&&(b==1))||((a==1)&&(b==0))) c = 2'b01;
	    if ((a==1)&&(b==1)) c = 2'b10;
	    return c;
	  endfunction
	  
	  function Bit#(130) fn_CLA_130(Bit#(130) op1, Bit#(130) op2);   //Carry Look Ahead Adder for 130 bit inputs, implemented using recursive doubling technique
      
      Vector#(131, Bit#(2)) carry_1;
      Bit#(130) carry,out;
      Bit#(131) c1,c0;
      
      for (Integer i=0; i<130; i=i+1) begin           //generating the carry from the input operands
        carry_1[i+1] = carry_gen(op1[i],op2[i]);
        end
      carry_1[0] = 2'b00;
      
      for (Integer i=0; i<131; i=i+1) begin           //splitting the carry vector into two single bit arrays so that we can perform bitwise operations
        c1[i] = carry_1[i][1];                        //performing bitwise operations is essential, all other type of implementations were tried and were not compiling smoothly.
        c0[i] = carry_1[i][0];
        end
      
      /* this is the carry propagation done in bitwise manner
         carry propagation rules:
         kill      * (kill/propagate/generate) = kill
         propagate * (kill/propagate/generate) = (kill/propagate/generate)
         generate  * (kill/propagate/generate) = generate
         these were converted into bit forms (kill : 00 propagate: 01 generate: 10) and derived sum of products form for the carry output and performed in a bitwise manner.
         the second input is carry vector shifted by n bits.
         n = 1,2,4,8,16,32,64,128
         This is because of the recursive doubling technique used in implementation of the CLA.
      */
      
      c1[130:1] = ((~c1[130:1])&c0[130:1]&c1[129:0])|(c1[130:1]&(~c0[130:1]));   //n=1 propagated from adjacent carries
      c0[130:1] = ((~c1[130:1])&c0[130:1]&c0[129:0]);     
                            
        
      for (Integer i=1; i<2; i=i+1) begin  // carry[1] is not altered again in the algorithm, so storing carry[1] in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
      
      c1[130:2] = ((~c1[130:2])&c0[130:2]&c1[128:0])|(c1[130:2]&(~c0[130:2]));   //n=2 propagated from second adjacent carries
      c0[130:2] = ((~c1[130:2])&c0[130:2]&c0[128:0]);
      
      for (Integer i=2; i<4; i=i+1) begin  // carry[3:2] is not altered again in the algorithm, so storing them in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
      
      c1[130:4] = ((~c1[130:4])&c0[130:4]&c1[126:0])|(c1[130:4]&(~c0[130:4]));   //n=4 propagated from fourth adjacent carries
      c0[130:4] = ((~c1[130:4])&c0[130:4]&c0[126:0]);
      
      for (Integer i=4; i<8; i=i+1) begin  // carry[7:4] is not altered again in the algorithm, so storing them in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
      
      c1[130:8] = ((~c1[130:8])&c0[130:8]&c1[122:0])|(c1[130:8]&(~c0[130:8]));   //n=8 propagated from eigth adjacent carries
      c0[130:8] = ((~c1[130:8])&c0[130:8]&c0[122:0]);
      
      for (Integer i=8; i<16; i=i+1) begin // carry[15:8] is not altered again in the algorithm, so storing them in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
      
      c1[130:16] = ((~c1[130:16])&c0[130:16]&c1[114:0])|(c1[130:16]&(~c0[130:16]));   //n=16 propagated from sixteenth adjacent carries
      c0[130:16] = ((~c1[130:16])&c0[130:16]&c0[114:0]);
      
      for (Integer i=16; i<32; i=i+1) begin // carry[31:16] is not altered again in the algorithm, so storing them in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
      
      c1[130:32] = ((~c1[130:32])&c0[130:32]&c1[98:0])|(c1[130:32]&(~c0[130:32]));    //n=32 propagated from 32nd adjacent carries
      c0[130:32] = ((~c1[130:32])&c0[130:32]&c0[98:0]);
      
      for (Integer i=32; i<64; i=i+1) begin  // carry[63:32] is not altered again in the algorithm, so storing them in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
        
      c1[130:64] = ((~c1[130:64])&c0[130:64]&c1[66:0])|(c1[130:64]&(~c0[130:64]));    //n=64 propagated from 64th adjacent carries
      c0[130:64] = ((~c1[130:64])&c0[130:64]&c0[66:0]);
      
      for (Integer i=64; i<128; i=i+1) begin  // carry[127:64] is not altered again in the algorithm, so storing them in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
        
      c1[130:128] = ((~c1[130:128])&c0[130:128]&c1[2:0])|(c1[130:128]&(~c0[130:128])); //n=128 propagated from 128th adjacent carries
      c0[130:128] = ((~c1[130:128])&c0[130:128]&c0[2:0]);
      
      for (Integer i=128; i<131; i=i+1) begin  // carry[130:128] is not altered again in the algorithm, so storing them in the variable
        carry_1[i][1] = c1[i];
        carry_1[i][0] = c0[i];
        end
     
      for (Integer i=0; i<130; i=i+1) begin                //finally calculating the carry bits from the kill/propagate/generate variables after the carry propagation is complete
        carry[i] = carry_1[i+1][0]|carry_1[i+1][1];
        end
      
      carry=carry<<1;  //carry should be shifted because it is generated for its neighbouring bit
	    
	    out = op1^op2^carry;  //final calculation of sum 
	    return out;
	    
	  endfunction
    
    
	  
	  rule rl_partial_production_reduction_tree_1;
	  
	    Bit#(67) csa_l1_1[21],csa_l1_2[21];    //Variables for storing CSA outputs from first reduction stage
      //l1_1 for storing sums and l1_2 for carries
      
      //First Reduction Stage
	    //first stage inputs are the direct partial products generated by ANDing one input by each bit of other input
	    for (Integer i=0; i<21; i=i+1) begin
	      csa_l1_1[i] = csa_65_1((rg_operands1[0]&signExtend(rg_operands2[0][3*i+2])),(rg_operands1[0]&signExtend(rg_operands2[0][3*i+1])),(rg_operands1[0]&signExtend(rg_operands2[0][3*i])))[0];
	      csa_l1_2[i] = csa_65_1((rg_operands1[0]&signExtend(rg_operands2[0][3*i+2])),(rg_operands1[0]&signExtend(rg_operands2[0][3*i+1])),(rg_operands1[0]&signExtend(rg_operands2[0][3*i])))[1];
	      end
	  
	    //second reduction stage
	    
	    Bit#(73) csa_l2_1[7],csa_l2_2[7],csa_l2_3[7],csa_l2_4[7];       //Variables for storing CSA outputs from second reduction stage
	    //l2_1 and l2_2 for sum and carry of l2_3 and l2_4 for sum and carry for l1_2 
	    
	    for (Integer i=0; i<7; i=i+1) begin
	      csa_l2_1[i] = csa_67_3 (csa_l1_1[3*i+2],csa_l1_1[3*i+1],csa_l1_1[3*i])[0];
	      csa_l2_2[i] = csa_67_3 (csa_l1_2[3*i+2],csa_l1_2[3*i+1],csa_l1_2[3*i])[0];
	      csa_l2_3[i] = csa_67_3 (csa_l1_1[3*i+2],csa_l1_1[3*i+1],csa_l1_1[3*i])[1];
	      csa_l2_4[i] = csa_67_3 (csa_l1_2[3*i+2],csa_l1_2[3*i+1],csa_l1_2[3*i])[1];
	      end
	    
	    Bit#(74) csa_l3_1[7],csa_l3_2[7];                               //variables for storing CSA outputs from third reduction stage
	    Bit#(91) csa_l3_3[2],csa_l3_4[2];
	    Bit#(76) csa_l3_5[2];
	    
	    for (Integer i=0; i<7; i=i+1) begin
	      csa_l3_1[i] = csa_73_0 (csa_l2_1[i],csa_l2_2[i],csa_l2_3[i])[0];
	      csa_l3_2[i] = csa_73_0 (csa_l2_1[i],csa_l2_2[i],csa_l2_3[i])[1];
	      end
	    csa_l3_3[0] = csa_73_9 (csa_l2_4[2],csa_l2_4[1],csa_l2_4[0])[0];
	    csa_l3_4[0] = csa_73_9 (csa_l2_4[2],csa_l2_4[1],csa_l2_4[0])[1];
	    csa_l3_3[1] = csa_73_9 (csa_l2_4[5],csa_l2_4[4],csa_l2_4[3])[0];
	    csa_l3_4[1] = csa_73_9 (csa_l2_4[5],csa_l2_4[4],csa_l2_4[3])[1];
	    
	    //Carry Save Addition (producing sum & carry)
	    csa_l3_5[0] = {3'b0,csa_l2_4[6]}^{2'b0,(rg_operands1[0]&signExtend(rg_operands2[0][63])),9'd0}^{1'b0,(rg_operands1[0]&signExtend(rg_operands2[0][64])),10'd0};
	    csa_l3_5[1] = ({3'b0,csa_l2_4[6]}&{2'b0,(rg_operands1[0]&signExtend(rg_operands2[0][63])),9'd0}|{2'b0,(rg_operands1[0]&signExtend(rg_operands2[0][63])),9'd0}&{1'b0,(rg_operands1[0]&signExtend(rg_operands2[0][64])),10'd0}|{3'b0,csa_l2_4[6]}&{1'b0,(rg_operands1[0]&signExtend(rg_operands2[0][64])),10'd0})<<1;
	    
	    rg_csa_l3_5_1 <= csa_l3_5[1];
	    rg_csa_l3_5_0 <= csa_l3_5[0];
	    Bit#(92) csa_l4_1[2],csa_l4_2[2],csa_l4_3[2],csa_l4_4[2];       //variables for storing CSA outputs from fourth reduction stage
	    Bit#(91) csa_l4_5[2];
	    Bit#(119) csa_l4_6[2];
	    
	    csa_l4_1[0] = csa_74_9 (csa_l3_1[3],csa_l3_1[2],csa_l3_1[1])[0];
	    csa_l4_2[0] = csa_74_9 (csa_l3_1[3],csa_l3_1[2],csa_l3_1[1])[1];
	    csa_l4_3[0] = csa_74_9 (csa_l3_2[3],csa_l3_2[2],csa_l3_2[1])[0];
	    csa_l4_4[0] = csa_74_9 (csa_l3_2[3],csa_l3_2[2],csa_l3_2[1])[1];
	    
	    csa_l4_1[1] = csa_74_9 (csa_l3_1[6],csa_l3_1[5],csa_l3_1[4])[0];
	    csa_l4_2[1] = csa_74_9 (csa_l3_1[6],csa_l3_1[5],csa_l3_1[4])[1];
	    csa_l4_3[1] = csa_74_9 (csa_l3_2[6],csa_l3_2[5],csa_l3_2[4])[0];
	    csa_l4_4[1] = csa_74_9 (csa_l3_2[6],csa_l3_2[5],csa_l3_2[4])[1];
	    
	    //Carry Save Addition (producing sum & carry)
	    
	    csa_l4_5[0] = {17'b0,csa_l3_1[0]}^{17'b0,csa_l3_2[0]}^csa_l3_3[0];
	    csa_l4_5[1] = ({17'b0,csa_l3_1[0]}&{17'b0,csa_l3_2[0]}|{17'b0,csa_l3_2[0]}&csa_l3_3[0]|{17'b0,csa_l3_1[0]}&csa_l3_3[0])<<1;
	    
	    csa_l4_6[0] = {1'b0,csa_l3_3[1],27'd0}^{1'b0,csa_l3_4[1],27'd0}^{28'b0,csa_l3_4[0]};
	    csa_l4_6[1] = {{csa_l3_3[1],27'd0}&{csa_l3_4[1],27'd0}|{csa_l3_4[1],27'd0}&{27'b0,csa_l3_4[0]}|{csa_l3_3[1],27'd0}&{27'b0,csa_l3_4[0]},1'b0};
	    
	    //fifth reduction stage, outputs stored in registers for carrying them to next pipeline stage
	    
	    csa_l5_1[0] <= {1'b0,csa_l4_1[0]^csa_l4_2[0]^csa_l4_3[0]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_2[0] <= {(csa_l4_1[0]&csa_l4_2[0])|(csa_l4_2[0]&csa_l4_3[0])|(csa_l4_1[0]&csa_l4_3[0]),1'b0};
	    
	    csa_l5_1[1] <= {1'b0,csa_l4_1[1]^csa_l4_2[1]^csa_l4_3[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_2[1] <= {(csa_l4_1[1]&csa_l4_2[1])|(csa_l4_2[1]&csa_l4_3[1])|(csa_l4_1[1]&csa_l4_3[1]),1'b0};
	    
	    csa_l5_3[0] <= csa_l4_6[0]^{28'b0,csa_l4_5[0]}^{28'b0,csa_l4_5[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_3[1] <= (csa_l4_6[0]&{28'b0,csa_l4_5[0]}|{28'b0,csa_l4_5[0]}&{28'b0,csa_l4_5[1]}|csa_l4_6[0]&{28'b0,csa_l4_5[1]})<<1;
	    
	    csa_l5_4[0] <= {27'b0,csa_l4_4[0],9'b0}^{csa_l4_4[1],36'b0}^{9'b0,csa_l4_6[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l5_4[1] <= ({27'b0,csa_l4_4[0],9'b0}&{csa_l4_4[1],36'b0}|{csa_l4_4[1],36'b0}&{9'b0,csa_l4_6[1]}|{27'b0,csa_l4_4[0],9'b0}&{9'b0,csa_l4_6[1]})<<1;
      
	  endrule
	  
	  rule rl_partial_production_reduction_tree_2;
	  
	    //sixth reduction stage
	    
	    //Variables for storing CSA outputs from sixth reduction stage
      Bit#(120) csa_l6_1[2];
      Bit#(129) csa_l6_2[2];
      Bit#(130) csa_l6_3[2];
	    
	    csa_l6_1[0] = {csa_l5_1[1],27'b0}^{27'b0,csa_l5_1[0]}^{27'b0,csa_l5_2[0]}; //Carry Save Addition (producing sum & carry)
	    csa_l6_1[1] = ({csa_l5_1[1],27'b0}&{27'b0,csa_l5_1[0]}|{27'b0,csa_l5_1[0]}&{27'b0,csa_l5_2[0]}|{csa_l5_1[1],27'b0}&{27'b0,csa_l5_2[0]})<<1;
	    
	    csa_l6_2[0] = {csa_l5_2[1],36'b0}^{10'b0,csa_l5_3[0]}^{10'b0,csa_l5_3[1]}; //Carry Save Addition (producing sum & carry)
	    csa_l6_2[1] = ({csa_l5_2[1],36'b0}&{10'b0,csa_l5_3[0]}|{10'b0,csa_l5_3[0]}&{10'b0,csa_l5_3[1]}|{csa_l5_2[1],36'b0}&{10'b0,csa_l5_3[1]})<<1;
	    
	    csa_l6_3[0] = {2'b0,csa_l5_4[0]}^{2'b0,csa_l5_4[1]}^{rg_csa_l3_5_0,54'd0}; //Carry Save Addition (producing sum & carry)
	    csa_l6_3[1] = ({2'b0,csa_l5_4[0]}&{2'b0,csa_l5_4[1]}|{2'b0,csa_l5_4[1]}&{rg_csa_l3_5_0,54'd0}|{2'b0,csa_l5_4[0]}&{rg_csa_l3_5_0,54'd0})<<1;
	    
	    rg_funct3_1 <= rg_funct3_0 ;
      rg_word_1 <= rg_word ;
      rg_valid_1 <= rg_valid ;
      rg_sign_1 <= rg_sign;
	    
	    Bit#(130) csa_l7_1[2],csa_l7_2[2];                               //variables for storing CSA outputs from seventh reduction stage
	    
	    csa_l7_1[0] = {1'b0,csa_l6_1[0],9'b0}^{1'b0,csa_l6_1[1],9'b0}^{1'b0,csa_l6_2[0]}; //Carry Save Addition (producing sum & carry)
	    csa_l7_1[1] = ({1'b0,csa_l6_1[0],9'b0}&{1'b0,csa_l6_1[1],9'b0}|{1'b0,csa_l6_1[1],9'b0}&{1'b0,csa_l6_2[0]}|{1'b0,csa_l6_1[0],9'b0}&{1'b0,csa_l6_2[0]})<<1;
	    
	    csa_l7_2[0] = {1'b0,csa_l6_2[1]}^csa_l6_3[0]^csa_l6_3[1]; //Carry Save Addition (producing sum & carry)
	    csa_l7_2[1] = ({1'b0,csa_l6_2[1]}&csa_l6_3[0]|csa_l6_3[0]&csa_l6_3[1]|{1'b0,csa_l6_2[1]}&csa_l6_3[1])<<1;
	    
	    Bit#(130) csa_l8[2];                                             //variables for storing CSA outputs from eigth reduction stage
	    
	    csa_l8[0] = csa_l7_1[0]^csa_l7_1[1]^csa_l7_2[0]; //Carry Save Addition (producing sum & carry)
	    csa_l8[1] = (csa_l7_1[0]&csa_l7_1[1]|csa_l7_1[1]&csa_l7_2[0]|csa_l7_1[0]&csa_l7_2[0])<<1;
	    
	    Bit#(130) csa_l9[2];                                             //variables for storing CSA outputs from ninth reduction stage
	    
	    csa_l9[0] = csa_l8[0]^csa_l8[1]^csa_l7_2[1]; //Carry Save Addition (producing sum & carry)
	    csa_l9[1] = (csa_l8[0]&csa_l8[1]|csa_l8[1]&csa_l7_2[1]|csa_l8[0]&csa_l7_2[1])<<1;
	    
	    //tenth reduction stage
	    
	    csa_l10[0] <= csa_l9[0]^csa_l9[1]^{rg_csa_l3_5_1,54'b0}; //Carry Save Addition (producing sum & carry)
	    csa_l10[1] <= (csa_l9[0]&csa_l9[1]|csa_l9[1]&{rg_csa_l3_5_1,54'b0}|csa_l9[0]&{rg_csa_l3_5_1,54'b0})<<1;
	    
	    rg_funct3_2 <= rg_funct3_1 ;
      rg_word_2 <= rg_word_1 ;
      rg_valid_2 <= rg_valid_1 ;
      rg_sign_2 <= rg_sign_1;
	  endrule
	  
	  rule rl_partial_production_reduction_tree_3;
	    
	    rg_out <= fn_CLA_130(csa_l10[0],csa_l10[1]);                 //Addition using Carry Look Ahead Adder
	    
	    rg_fn3 <= rg_funct3_2 ;
      rg_word_3 <= rg_word_2 ;
      rg_valid_3 <= rg_valid_2 ;
      rg_sign_3 <= rg_sign_2;
	  endrule
	  
	  //sending operands after converting them to positive operands, sending opcode
    method Action send(Bit#(`XLEN) in1, Bit#(`XLEN) in2, Bit#(3) funct3
                                                              `ifdef RV64, Bool word32 `endif );
      Bit#(1) sign1 = funct3[1]^funct3[0];
      Bit#(1) sign2 = pack(funct3[1 : 0] == 1);
      let op1 = unpack({sign1 & in1[valueOf(`XLEN) - 1], in1});
      let op2 = unpack({sign2 & in2[valueOf(`XLEN) - 1], in2});
      
      Bit#(65) opp1 =0;
      Bit#(65) opp2 =0;
      
      //sending the positive version of operands (making them positive if they are negative)
      if ((sign1 & in1[valueOf(`XLEN) - 1])==0) begin opp1 = op1; end
      else begin opp1 = (~op1)+65'd1; end
      if ((sign2 & in2[valueOf(`XLEN) - 1])==0) begin opp2 = op2; end
      else begin opp2 = (~op2)+65'd1; end
      
      rg_funct3_0 <= funct3;
      
      //determining the sign of the final product to correct it in the final stage
      rg_sign <= (sign2 & in2[valueOf(`XLEN) - 1])^(sign1 & in1[valueOf(`XLEN) - 1]);
    `ifdef RV64
      rg_word <= word32;
    `endif
     rg_valid<=1;
     
     rg_operands1[0] <= opp1;
     rg_operands2[0] <= opp2;
    
    endmethod
    
    //Giving appropriate sign to the product, determining which bits to send, method for receiving the output and valid bit
    method Tuple2#(Bit#(1),Bit#(64)) receive;
    
      Bool lv_upperbits = unpack(|rg_fn3[1:0]);  //determining whether the upper XLEN bits or lower XLEN bits to send according to the funct3
      
      Bit#(`XLEN) default_out;
      Bit#(130) out;
      
      if (rg_sign_3==1)
        out = ~rg_out+1;
      else
        out = rg_out;
      
      if (lv_upperbits) begin
        default_out = out[valueOf(TMul#(2, `XLEN)) - 1 : valueOf(`XLEN)];
        end
      else
        default_out = out[valueOf(`XLEN) - 1:0];

      `ifdef RV64            //implementing RV64 MULW
        if(rg_word_3)
          default_out = signExtend(default_out[31 : 0]);
      `endif
      return tuple2(rg_valid_3,default_out);
    endmethod
    
  endmodule
endpackage
